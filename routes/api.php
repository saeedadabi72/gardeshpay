<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// auth
Route::post('auth/login', 'AuthController@login')->name('login');
Route::post('auth/register', 'AuthController@register');

// role
Route::post('role', 'RoleController@create');
Route::post('role/{id}/permissions', 'RoleController@assignPermissions');

// user
Route::post('user/{id}/roles', 'UserController@assignRoles');

