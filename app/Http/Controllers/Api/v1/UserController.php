<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public $userService;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:super-admin|admin');
        $this->userService = new AuthService();
    }

    public function assignRoles($id, Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:users,id',
            'roles' => 'required|array|exists:roles,id',
        ];

        $data = $request->all();
        $data['id'] = $id;

        if($validate = $this->validateJson($data,$rules)) {
            return $validate;
        }

        $this->userService->assignRole($id, $request->roles);

        return $this->json([]);
    }
}
