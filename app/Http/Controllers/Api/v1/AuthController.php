<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public $authService;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
        $this->middleware('role:super-admin|admin', ['only' => ['assignRoles']]);
        $this->authService = new AuthService();
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'
        ];

        if($validate = $this->validateJson($request->all(),$rules)) {
            return $validate;
        }

        try {
            $this->authService->register($request->all());
            return $this->json([],'success',0, 'user created successfully');
        } catch (\Exception $e) {
            return $this->json([], 'failed', 1, $e->getMessage());
        }
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6',
        ];

        if($validate = $this->validateJson($request->all(),$rules)) {
            return $validate;
        }

        if (! $token = $this->authService->login($request->all())) {
            return $this->json([], 'failed', 1, 'Unauthorized', 401);
        }

        return $this->json($this->respondWithToken($token));
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

}
