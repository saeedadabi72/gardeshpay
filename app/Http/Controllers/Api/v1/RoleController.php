<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->middleware('auth:api');
        $this->roleService = $roleService;
    }

    public function create(Request $request)
    {
        $rules = ['name' => 'required|string|unique:roles'];

        if($validate = $this->validateJson($request->all(),$rules)) {
            return $validate;
        }

        $this->roleService->create($request->name);

        return $this->json([]);
    }

    public function assignPermissions($id, Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:roles,id',
            'permissions' => 'required|array|exists:permissions,id',
        ];

        $data = $request->all();
        $data['id'] = $id;

        if($validate = $this->validateJson($data,$rules)) {
            return $validate;
        }

        $this->roleService->assignPermissionToRole($id, $request->permissions);

        return $this->json([]);
    }


}
