<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function json($data, $status = 'success', $code = 0, $message = '', $statusCode = 200, array $headers = [])
    {
        return response()->json([
            'status' => $status,
            'code' => $code,
            'data' => $data,
            'message' => $message
        ],
            $statusCode,
            $headers);
    }

    protected function validateJson($data, $rules, $messages = [], $customAttributes = [])
    {
        $validator = Validator::make($data, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'validation',
                'message' => $validator->errors()
            ]);
        }

        return false;
    }
}
