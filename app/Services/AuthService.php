<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository(new User());
    }

    public function register($data)
    {
        return $this->userRepository->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function login($credentials)
    {
        return auth()->attempt($credentials);
    }
}
