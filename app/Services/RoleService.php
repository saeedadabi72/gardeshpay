<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Spatie\Permission\Models\Role;


class RoleService
{
    public $roleRepository;

    public function __construct()
    {
        $this->roleRepository = new RoleRepository(new Role());
    }

    public function create($name)
    {
        $this->roleRepository->create(['name' => $name]);
    }

    public function assignPermissionToRole($id, $permissions)
    {
        $role = $this->roleRepository->show($id);
        $role->syncPermissions($permissions);
    }
}
