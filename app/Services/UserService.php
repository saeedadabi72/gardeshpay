<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;

class UserService
{
    public $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository(new User());
    }

    public function assignRole($id, $roles)
    {
        $user = $this->userRepository->show($id);
        $user->assignRole($roles);
    }
}
